namespace Todo {
    string get_todos_path () {
        return Path.build_filename (Environment.get_user_data_dir (), "vala-tutorial-todo", "todos.json");
    }

    int main (string[] args) {
        var todos_file = File.new_for_path (get_todos_path ());
        if (!todos_file.query_exists ()) {
            try {
                var dir = todos_file.get_parent ();
                if (!dir.query_exists ()) {
                    dir.make_directory_with_parents ();
                }
                var stream = todos_file.create (FileCreateFlags.NONE, null);
                stream.write ("[]".data, null);
                stream.close ();
            } catch (Error e) {
                error ("Error creating todos.json file: %s", e.message);
            }
        }

        try {
            TodoController todos = new TodoController ();
            todos.add_todo (new Todo ("a", "b", "2022-08-03T00:00:00"));
        } catch (Error e) {
            error ("Error: %s", e.message);
        }
        return 0;
    }
}
