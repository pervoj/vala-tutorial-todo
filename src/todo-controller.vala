class Todo.TodoController {
    private List<Todo> todos = new List<Todo> ();

    public TodoController () throws TodoError, Error {
        load_todos ();
    }

    public void add_todo (Todo todo) throws TodoError, Error {
        todos.append (todo);
        save_todos ();
    }

    public void load_todos () throws TodoError, Error {
        todos = new List<Todo> ();

        var parser = new Json.Parser ();
        parser.load_from_file (get_todos_path ());

        var variant = Json.gvariant_deserialize (parser.get_root (), null);

        VariantIter iter = variant.iterator ();
        Variant? val;
        while ((val = iter.next_value ()) != null) {
            var todo = new Todo.from_variant (val.get_variant ());
            todos.append (todo);
        }
    }

    public void save_todos () throws TodoError, Error {
        VariantBuilder builder = new VariantBuilder (new VariantType ("av"));
        foreach (Todo todo in todos) {
            builder.add_value (new Variant.variant (todo.to_variant ()));
        }
        var root_node = Json.gvariant_serialize (builder.end ());
        var generator = new Json.Generator ();
        generator.pretty = true;
        generator.set_root (root_node);
        generator.to_file (get_todos_path ());
    }
}
