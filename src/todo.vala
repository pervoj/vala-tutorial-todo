errordomain Todo.TodoError {
    VARIANT_PARSING_FAILED,
    DATE_PARSING_FAILED
}

class Todo.Todo {
    public string title { get; private set; }
    public string description { get; private set; }
    public DateTime? date { get; private set; }

    private string variant_type = "a{sv}";

    public Todo (string title, string description, string date) throws TodoError {
        this.title = title;
        this.description = description;
        this.date = string_to_date (date);

        if (this.date == null) {
            throw new TodoError.DATE_PARSING_FAILED (
                "date parsing failed for %s", date
            );
        }
    }

    public Todo.from_variant (Variant variant) throws TodoError {
        // check the variant type
        string current_type = variant.get_type_string ();
        if (current_type != variant_type) {
            throw new TodoError.VARIANT_PARSING_FAILED (
                "the variant type is %s, expected %s",
                current_type, variant_type
            );
        }

        // parse the variant
        VariantIter iter = variant.iterator ();

        string date_string = "";

        string key;
        Variant val_variant;
        while (iter.next ("{sv}", out key, out val_variant)) {
            string val = val_variant.get_string ();
            switch (key) {
                case "title":
                    title = val;
                    break;
                case "description":
                    description = val;
                    break;
                case "date":
                    date_string = val;
                    date = string_to_date (date_string);
                    break;
            }
        }

        if (date == null) {
            throw new TodoError.DATE_PARSING_FAILED (
                "date parsing failed for %s", date_string
            );
        }
    }

    public DateTime? string_to_date (string s) {
        return new DateTime.from_iso8601 (s, new TimeZone.local ());
    }

    public Variant to_variant () {
        VariantBuilder builder = new VariantBuilder (new VariantType (variant_type));
        builder.add ("{sv}", "title", new Variant.string (title));
        builder.add ("{sv}", "description", new Variant.string (description));
        builder.add ("{sv}", "date", new Variant.string (date.format_iso8601 ()));
        return builder.end ();
    }
}
